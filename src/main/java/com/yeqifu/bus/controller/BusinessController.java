package com.yeqifu.bus.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yeqifu.bus.entity.Goods;
import com.yeqifu.bus.service.IGoodsService;
import com.yeqifu.bus.vo.GoodsVo;
import com.yeqifu.sys.common.Constast;
import com.yeqifu.sys.common.DataGridView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 业务管理的路由器
 * @Author: 落亦-
 * @Date: 2019/12/5 9:33
 */
@Controller
@RequestMapping("bus")
public class BusinessController {
    @Autowired
    private IGoodsService goodsService;

    /**
     * 跳转到客户管理页面
     * @return
     */
    @RequestMapping("toCustomerManager")
    public String toCustomerManager(){
        return "business/customer/customerManager";
    }

    /**
     * 跳转到供应商管理页面
     * @return
     */
    @RequestMapping("toProviderManager")
    public String toProviderManager(){
        return "business/provider/providerManager";
    }

    /**
     * 跳转到商品管理页面
     * @return
     */
    @RequestMapping("toGoodsManager")
    public String toGoodsManager(){
        return "business/goods/goodsManager";
    }

    /**
     * 跳转到进货管理页面
     * @return
     */
    @RequestMapping("toInportManager")
    public String toInportManager(){
        return "business/inport/inportManager";
    }

    /**
     * 跳转到退货管理页面
     * @return
     */
    @RequestMapping("toOutportManager")
    public String toOutportManager(){
        return "business/outport/outportManager";
    }

    /**
     * 跳转到商品销售管理页面
     * @return
     */
    @RequestMapping("toSalesManager")
    public String toSalesManager(){
        return "business/sales/salesManager";
    }

    /**
     * 跳转到商品销售管理页面
     * @return
     */
    @RequestMapping("toSalesbackManager")
    public String toSalesbackManager(){
        return "business/salesback/salesbackManager";
    }

    /**
     * 跳转到商品销售管理页面
     * @return
     */
    @RequestMapping("toBuyManager")
    public String toBuyManager(Model model){
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<Goods>();
        queryWrapper.eq("available", Constast.AVAILABLE_TRUE);
        List<Goods> list = goodsService.list(queryWrapper);
        model.addAttribute("goods",list);
        return "business/buy/buyManager"; }

    @RequestMapping("toBuy")
    public String toBuy(@RequestParam("name") String name,@RequestParam("description") String description,Model model){
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<Goods>();
        queryWrapper.like("goodsname",name);
        queryWrapper.like("description",description);
        queryWrapper.eq("available", Constast.AVAILABLE_TRUE);
        List<Goods> list = goodsService.list(queryWrapper);
        model.addAttribute("goods",list);
        return "business/buy/buyManager";
    }
    @RequestMapping("toMyOrder")
    public String toMyOrder(){
        return "business/order/order";
    }
    @RequestMapping("toMyCar")
    public String toMyCar(){
        return "business/order/car";
    }
    @RequestMapping("toCaigou")
    public String toCaigou(){
        return "business/inport/caigou";
    }

    @RequestMapping("toCaiwu")
    public String toCaiwu(){
        return "business/sales/caiwu";
    }

    @RequestMapping("toRepair")
    public String toRepair(){
        return "business/order/order";
    }

}