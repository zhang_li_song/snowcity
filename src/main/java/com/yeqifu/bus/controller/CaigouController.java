package com.yeqifu.bus.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yeqifu.bus.entity.Goods;
import com.yeqifu.bus.entity.Caigou;
import com.yeqifu.bus.entity.Provider;
import com.yeqifu.bus.mapper.CaigouMapper;
import com.yeqifu.bus.service.*;
import com.yeqifu.bus.service.CaigouService;
import com.yeqifu.bus.vo.CaigouVo;
import com.yeqifu.bus.vo.InportVo;
import com.yeqifu.sys.common.Constast;
import com.yeqifu.sys.common.DataGridView;
import com.yeqifu.sys.common.ResultObj;
import com.yeqifu.sys.common.WebUtils;
import com.yeqifu.sys.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-02-18
 */
@RestController
@RequestMapping("/caigou")
public class CaigouController {

    @Autowired
    private CaigouService caigouService;

    @Autowired
    private CaigouMapper caigouMapper;

    @Autowired
    private IProviderService providerService;

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private IInportService inportService;

    /**
     * 查询商品进货
     * @param caigouVo
     * @return
     */
    @RequestMapping("loadAllCaigou")
    public DataGridView loadAllCaigou(CaigouVo caigouVo){
        IPage<Caigou> page = new Page<Caigou>(caigouVo.getPage(),caigouVo.getLimit());
        QueryWrapper<Caigou> queryWrapper = new QueryWrapper<Caigou>();
        //对供应商进行查询
        queryWrapper.eq(caigouVo.getState()!=null,"state",caigouVo.getState());

        queryWrapper.eq(caigouVo.getProviderid()!=null&&caigouVo.getProviderid()!=0,"providerid",caigouVo.getProviderid());
        //对商品进行查询
        queryWrapper.eq(caigouVo.getGoodsid()!=null&&caigouVo.getGoodsid()!=0,"goodsid",caigouVo.getGoodsid());
        //对时间进行查询要求大于开始时间小于结束时间
        queryWrapper.ge(caigouVo.getStartTime()!=null,"inporttime",caigouVo.getStartTime());
        queryWrapper.le(caigouVo.getEndTime()!=null,"inporttime",caigouVo.getEndTime());
        //通过进货时间对商品进行排序
        queryWrapper.orderByDesc("inporttime");
        IPage<Caigou> page1 = caigouService.page(page, queryWrapper);
        List<Caigou> records = page1.getRecords();
        for (Caigou caigou : records) {
            Provider provider = providerService.getById(caigou.getProviderid());
            if (provider!=null){
                //设置供应商姓名
                caigou.setProvidername(provider.getProvidername());
            }
            Goods goods = goodsService.getById(caigou.getGoodsid());
            if (goods!=null){
                //设置商品名称
                caigou.setGoodsname(goods.getGoodsname());
                //设置商品规格
                caigou.setSize(goods.getSize());
            }
        }
        return new DataGridView(page1.getTotal(),page1.getRecords());
    }


    /**
     * 添加进货商品
     * @param caigouVo
     * @return
     */
    @RequestMapping("addCaigou")
    public ResultObj addCaigou(CaigouVo caigouVo){
        try {
            //获得当前系统用户
            User user = (User) WebUtils.getSession().getAttribute("user");
            //设置操作人
            caigouVo.setOperateperson(user.getName());
            //设置进货时间
            caigouVo.setInporttime(new Date());
            caigouService.save(caigouVo);
            return ResultObj.ADD_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.ADD_ERROR;
        }
    }



    @RequestMapping("jinhuo")
    public ResultObj jinhuo(InportVo inportVo){
        try {
            Caigou caigou = new Caigou();
            caigou.setId(inportVo.getId());
            caigou.setState(4);
            caigouService.updateById(caigou);

            //获得当前系统用户
            User user = (User) WebUtils.getSession().getAttribute("user");
            //设置操作人
            inportVo.setOperateperson(user.getName());
            //设置进货时间
            inportVo.setInporttime(new Date());
            inportService.save(inportVo);


            return ResultObj.ADD_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.ADD_ERROR;
        }
    }

    /**
     * 更新进货商品
     * @param caigouVo
     * @return
     */
    @RequestMapping("updateCaigou")
    public ResultObj updateCaigou(CaigouVo caigouVo){
        try {
            caigouService.updateById(caigouVo);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }

    }

    /**
     * 删除进货商品
     * @param id
     * @return
     */
    @RequestMapping("deleteCaigou")
    public ResultObj deleteCaigou(Integer id){
        try {
            caigouService.removeById(id);
            return ResultObj.DELETE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.DELETE_ERROR;
        }
    }

    @RequestMapping("back")
    public ResultObj back(Integer id){
        try {
            caigouMapper.back(id);
            return new ResultObj(Constast.OK,"已退回");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.DELETE_ERROR;
        }
    }

    @RequestMapping("pass")
    public ResultObj pass(Integer id){
        try {
            caigouMapper.pass(id);
            return new ResultObj(Constast.OK,"已通过");
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.DELETE_ERROR;
        }
    }


}

