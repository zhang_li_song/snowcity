package com.yeqifu.bus.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yeqifu.bus.entity.Car;
import com.yeqifu.bus.service.CarService;
import com.yeqifu.bus.vo.CarVo;
import com.yeqifu.bus.vo.OrderVo;
import com.yeqifu.sys.common.AppFileUtils;
import com.yeqifu.sys.common.DataGridView;
import com.yeqifu.sys.common.ResultObj;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/car")
public class CarController {
    
    
    @Autowired
    private CarService carService;
    
    
    @RequestMapping("loadAllCar")
    public DataGridView loadAllCar(CarVo carVo){
        IPage<Car> page = new Page<Car>(carVo.getPage(),carVo.getLimit());
        QueryWrapper<Car> queryWrapper = new QueryWrapper<Car>();
        if (carVo.getUsername()!=null) {
            queryWrapper.eq(!carVo.getUsername().contains("管理员"), "username", carVo.getUsername());
        }
        carService.page(page,queryWrapper);
        return new DataGridView(page.getTotal(),page.getRecords());
    }


    /**
     * 添加
     * @param
     * @return
     */
    @RequestMapping("addCar")
    public ResultObj addCar(CarVo carVo){
        try {
            carService.save(carVo);
            return ResultObj.ADD_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.ADD_ERROR;
        }
    }

    @RequestMapping("deleteCar")
    public ResultObj deleteCar(CarVo carVo){
        try {
            carService.removeById(carVo.getId());
            return ResultObj.DELETE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.DELETE_ERROR;
        }
    }
}

