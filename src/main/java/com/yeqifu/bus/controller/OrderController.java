package com.yeqifu.bus.controller;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yeqifu.bus.entity.Goods;
import com.yeqifu.bus.entity.Order;
import com.yeqifu.bus.entity.Provider;
import com.yeqifu.bus.entity.Sales;
import com.yeqifu.bus.mapper.OrderMapper;
import com.yeqifu.bus.service.IGoodsService;
import com.yeqifu.bus.mapper.GoodsMapper;
import com.yeqifu.bus.service.IOrderService;
import com.yeqifu.bus.service.ISalesService;
import com.yeqifu.bus.vo.GoodsVo;
import com.yeqifu.bus.vo.OrderVo;
import com.yeqifu.bus.vo.SalesVo;
import com.yeqifu.sys.common.*;
import com.yeqifu.sys.entity.User;
import com.yeqifu.sys.service.IUserService;
import com.yeqifu.sys.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author: Yao a Yao
 * @date: 2021/11/2
 */

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @Autowired
    private ISalesService salesService;

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private IUserService userService;

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 生成订单
     * @param orderVo
     * @return
     */
    @RequestMapping("CreateOrder")
    public ResultObj CreateOrder(OrderVo orderVo){
        SalesVo salesVo = new SalesVo();
        salesVo.setOrdernum("SN"+ UUID.randomUUID());
        orderVo.setOrdernum("SN"+ UUID.randomUUID());
        salesVo.setCustomername(orderVo.getUsername());
        salesVo.setSalestime(new Date());
        salesVo.setNumber(orderVo.getNum());
        salesVo.setRemark(orderVo.getDescription());
        salesVo.setSaleprice(orderVo.getPrice());
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<Goods>();
        queryWrapper.eq("goodsname",orderVo.getGoodsname());
        List<Goods> list = goodsService.list(queryWrapper);
        for (Goods goods : list) {
          salesVo.setGoodsid(goods.getId());
        }
        salesService.save(salesVo);

        orderVo.setCreatetime(new Date());
        orderService.save(orderVo);

        goodsMapper.reduce2(orderVo);

        User user = (User) WebUtils.getSession().getAttribute("user");
        int i = userService.reduceMoney(orderVo.getPrice(), user.getId());
        if(i<1){
            return ResultObj.MONEY_ERROR;
        }
        return ResultObj.ADD_SUCCESS;
    }
    /**
     * （租赁管理员/顾客）查询商品
     * @param orderVo
     * @return
     */
    @RequestMapping("loadAllOrder")
    public DataGridView loadAllOrder(OrderVo orderVo){
        List<String> list = new ArrayList<>();
        list.add("4");list.add("5");list.add("6");
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getGoodsname()),"goodsname",orderVo.getGoodsname());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getProduceplace()),"productplace",orderVo.getProduceplace());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getDescription()),"description",orderVo.getDescription());
        queryWrapper.in(StringUtils.isNotBlank(orderVo.getState()),"state", list);

        if (orderVo.getUsername()!=null) {
            queryWrapper.like(!orderVo.getUsername().contains("管理员"), "username", orderVo.getUsername());
        }
        queryWrapper.orderByDesc("id");
        orderService.page(page,queryWrapper);
        return new DataGridView(page.getTotal(),page.getRecords());
    }

    @RequestMapping("loadAllOrder2")
    public DataGridView loadAllOrder2(OrderVo orderVo){
        List<String> list = new ArrayList<>();
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getGoodsname()),"goodsname",orderVo.getGoodsname());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getProduceplace()),"productplace",orderVo.getProduceplace());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getDescription()),"description",orderVo.getDescription());
        queryWrapper.in(StringUtils.isNotBlank(orderVo.getState()),"state", orderVo.getState());

        if (orderVo.getUsername()!=null) {
            queryWrapper.like(!orderVo.getUsername().contains("管理员"), "username", orderVo.getUsername());
        }
        queryWrapper.orderByDesc("id");
        orderService.page(page,queryWrapper);
        return new DataGridView(page.getTotal(),page.getRecords());
    }

    //销售管理员查询订单
    @RequestMapping("loadAllOrderSale")
    public DataGridView loadAllOrderSale(OrderVo orderVo){
        List<String> list = new ArrayList<>();
        list.add("1");list.add("2");list.add("3");
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        QueryWrapper<Order> queryWrapper = new QueryWrapper<Order>();
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getGoodsname()),"goodsname",orderVo.getGoodsname());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getProduceplace()),"productplace",orderVo.getProduceplace());
        queryWrapper.like(StringUtils.isNotBlank(orderVo.getDescription()),"description",orderVo.getDescription());
        queryWrapper.in(StringUtils.isBlank(orderVo.getState()),"state", list);
        queryWrapper.eq(StringUtils.isNotBlank(orderVo.getState()),"state", orderVo.getState());

        if (orderVo.getUsername()!=null) {
            queryWrapper.like(!orderVo.getUsername().contains("管理员"), "username", orderVo.getUsername());
        }
        queryWrapper.orderByDesc("id");
        orderService.page(page,queryWrapper);
        return new DataGridView(page.getTotal(),page.getRecords());
    }

    /**
     * 修改订单
     * @param
     * @return
     */
    @RequestMapping("updateOrder")
    public ResultObj updateOrder(OrderVo orderVo){
        try {
            orderService.updateById(orderVo);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }

    /**
     * 删除
     * @param
     * @return
     */
    @RequestMapping("deleteOrder/{id}")
    public ResultObj deleteOrder(@PathVariable("id") Integer id){
        try {
            orderService.removeById(id);
            return ResultObj.DELETE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.DELETE_ERROR;
        }
    }


    /**
     * 添加
     * @param
     * @return
     */
    @RequestMapping("addOrder")
    public ResultObj addOrder(OrderVo orderVo){
        try {
            orderService.save(orderVo);
            return ResultObj.ADD_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.ADD_ERROR;
        }
    }

    //确认收货
    @RequestMapping("/confirmGet")
    public ResultObj updateGoods(Integer id){
        try {
            orderService.updateById2(id);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }

    //发货
    @RequestMapping("/fahuo")
    public ResultObj fahuo(Integer id){
        try {
            orderService.updateById3(id);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }
    //归还
    @RequestMapping("/return")
    public ResultObj re_turn(Integer id){
        try {
            orderService.updateById5(id);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }

    //确认归还
    @RequestMapping("/confirmReturn")
    public ResultObj confirmReturn(Integer id){
        try {
            orderService.updateById6(id);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }

    //维护
    @RequestMapping("/repair")
    public ResultObj repair(Integer id){
        try {
            orderService.repair(id);
            return ResultObj.UPDATE_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return ResultObj.UPDATE_ERROR;
        }
    }

    //查询成本
    @RequestMapping("chengben")
    public DataGridView chengben(OrderVo orderVo){
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        List<OrderVo> List = orderMapper.chengben(orderVo.getStartTime(),orderVo.getEndTime());
        long count = (long)List.size();
        return new DataGridView(count,List);
    }
    //查询营业额
    @RequestMapping("yingyee")
    public DataGridView yingyee(OrderVo orderVo){
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        List<OrderVo> List = orderMapper.yingyee(orderVo.getStartTime(),orderVo.getEndTime());
        long count = (long)List.size();
        return new DataGridView(count,List);
    }
    //查询利润
    @RequestMapping("lirun")
    public DataGridView lirun(OrderVo orderVo){
        IPage<Order> page = new Page<Order>(orderVo.getPage(),orderVo.getLimit());
        List<OrderVo> List = orderMapper.lirun(orderVo.getStartTime(),orderVo.getEndTime());
        long count = (long)List.size();
        return new DataGridView(count,List);
    }
}
