package com.yeqifu.bus.service;

import com.yeqifu.bus.entity.Car;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-04-02
 */
public interface CarService extends IService<Car> {

}
