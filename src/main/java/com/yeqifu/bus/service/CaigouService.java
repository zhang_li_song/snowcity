package com.yeqifu.bus.service;

import com.yeqifu.bus.entity.Caigou;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-02-18
 */
public interface CaigouService extends IService<Caigou> {

}
