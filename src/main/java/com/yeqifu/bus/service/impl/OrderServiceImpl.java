package com.yeqifu.bus.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yeqifu.bus.entity.Order;
import com.yeqifu.bus.mapper.OrderMapper;
import com.yeqifu.bus.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: ZhangLiSong
 * @email: zls0451@163.com
 * @date: 2021/11/2
 */
@Component
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public boolean updateById2(Integer id) {
        return orderMapper.updateById2(id);
    }

    @Override
    public boolean updateById3(Integer id) {
        return orderMapper.updateById3(id);
    }

    @Override
    public boolean updateById5(Integer id) {
        return orderMapper.updateById5(id);
    }
    @Override
    public boolean updateById6(Integer id) {
        return orderMapper.updateById6(id);
    }

    @Override
    public boolean repair(Integer id) {
        return orderMapper.repair(id);
    }


}
