package com.yeqifu.bus.service.impl;

import com.yeqifu.bus.entity.Caigou;
import com.yeqifu.bus.mapper.CaigouMapper;
import com.yeqifu.bus.service.CaigouService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-02-18
 */
@Service
public class CaigouServiceImpl extends ServiceImpl<CaigouMapper, Caigou> implements CaigouService {

}
