package com.yeqifu.bus.service.impl;

import com.yeqifu.bus.entity.Car;
import com.yeqifu.bus.mapper.CarMapper;
import com.yeqifu.bus.service.CarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-04-02
 */
@Service
public class CarServiceImpl extends ServiceImpl<CarMapper, Car> implements CarService {

}
