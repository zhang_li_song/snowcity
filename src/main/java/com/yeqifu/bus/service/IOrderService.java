package com.yeqifu.bus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yeqifu.bus.entity.Order;

/**
 * @author: ZhangLiSong
 * @email: zls0451@163.com
 * @date: 2021/11/2
 */
public interface IOrderService extends IService<Order> {

    boolean updateById2(Integer id);

    boolean updateById3(Integer id);

    boolean updateById5(Integer id);

    boolean updateById6(Integer id);

    boolean repair(Integer id);

}
