package com.yeqifu.bus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yeqifu.bus.entity.Order;
import com.yeqifu.bus.vo.OrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author: ZhangLiSong
 * @email: zls0451@163.com
 * @date: 2021/11/2
 */
public interface OrderMapper extends BaseMapper<Order> {
    boolean updateById2(Integer id);

    boolean updateById3(Integer id);

    boolean updateById5(Integer id);

    boolean updateById6(Integer id);

    boolean repair(Integer id);

    List<OrderVo> chengben(@Param("startTime") String startTime, @Param("endTime") String endTime);


    List<OrderVo> yingyee(@Param("startTime") String startTime, @Param("endTime") String endTime);


    List<OrderVo> lirun(@Param("startTime") String startTime, @Param("endTime") String endTime);
}
