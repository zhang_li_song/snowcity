package com.yeqifu.bus.mapper;

import com.yeqifu.bus.entity.Caigou;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-02-18
 */
public interface CaigouMapper extends BaseMapper<Caigou> {

    Integer pass(Integer id);

    Integer back(Integer id);
}
