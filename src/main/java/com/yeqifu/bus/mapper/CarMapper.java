package com.yeqifu.bus.mapper;

import com.yeqifu.bus.entity.Car;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-04-02
 */
public interface CarMapper extends BaseMapper<Car> {

}
