package com.yeqifu.bus.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yeqifu.bus.entity.Order;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @date: 2021/11/2
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderVo extends Order {

    private Integer page=1;
    private Integer limit=10;

    //用于财务统计的变量
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date time;

    @JsonFormat(pattern = "yyyy-MM")
    private String time1;
    private Double money;


    @JsonFormat(pattern="yyyy-MM-dd")
    private String startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private String endTime;

}
