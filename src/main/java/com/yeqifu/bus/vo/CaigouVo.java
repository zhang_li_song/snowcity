package com.yeqifu.bus.vo;

import com.yeqifu.bus.entity.Caigou;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author:
 * @email:
 * @date: 2022/2/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CaigouVo extends Caigou {
    private Integer page = 1;

    private Integer limit = 10;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
}
