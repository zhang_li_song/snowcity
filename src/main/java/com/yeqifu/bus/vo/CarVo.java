package com.yeqifu.bus.vo;

import com.yeqifu.bus.entity.Caigou;
import com.yeqifu.bus.entity.Car;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author:
 * @email:
 * @date: 2022/2/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CarVo extends Car {
    private Integer page = 1;

    private Integer limit = 10;

}
